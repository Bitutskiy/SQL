CREATE DATABASE phonebook;

USE phonebook;

# Информация о контактах
CREATE TABLE `Contact` (
    `id` INT(10) NOT NULL AUTO_INCREMENT,
    `idCategory` INT(10) NOT NULL,
    `name` VARCHAR(60) NOT NULL UNIQUE,
    PRIMARY KEY (`id`)
);

# Доступные категории 
CREATE TABLE `Category` (
    `id` INT(10) NOT NULL UNIQUE,
    `name` VARCHAR(30) NOT NULL,
    PRIMARY KEY (`id`)
);

# Номера телефонов, привязанные к контакту
# может содержать несколько записей
CREATE TABLE `PhoneNumber` (
    `idContact` INT(10) NOT NULL,
    `number` varchar(11) NOT NULL ,
    PRIMARY KEY (`number`)
);

# Журнал вызовов
CREATE TABLE `Journal` (
    `number` varchar(11) NOT NULL,
    `idContact` INT(11) NOT NULL,
    `start` DATETIME NOT NULL,
    `end` DATETIME NOT NULL
);

# Внешнии ключи
ALTER TABLE `Contact` ADD CONSTRAINT `Contact_fk0` FOREIGN KEY (`idCategory`) REFERENCES `Category`(`id`);
ALTER TABLE `PhoneNumber` ADD CONSTRAINT `Number_fk0` FOREIGN KEY (`idContact`) REFERENCES `Contact`(`id`);
ALTER TABLE `Journal` ADD CONSTRAINT `Journal_fk0` FOREIGN KEY (`number`) REFERENCES `PhoneNumber`(`number`);
